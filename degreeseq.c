/**************************************************************************
 * Copyright (C) 2011 Minh Van Nguyen <nguyenminh2@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

#include <igraph.h>
#include <stdio.h>

/* The degree sequence of the network of scientists posting preprints to the
 * condensed matter category of arXiv.  We only consider the total degree
 * sequence as the network is undirected.  We also assume that the network
 * has no multiple edges nor self-loops, i.e. it is simple.
 */
int main() {
  FILE *file;
  igraph_t G;          /* the graph from which to obtain degrees */
  igraph_integer_t i;  /* general index */
  igraph_vector_t D;   /* the degree sequence */

  file = fopen("cond-mat-2005.gml", "r");
  igraph_read_graph_gml(&G, file);
  fclose(file);
  igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);

  /* total degree sequence */
  igraph_vector_init(&D, 0);
  igraph_degree(&G, &D, igraph_vss_all(), IGRAPH_ALL, IGRAPH_NO_LOOPS);
  file = fopen("totaldegree.seq", "w");
  for (i = 0; i < igraph_vector_size(&D); i++) {
    fprintf(file, "%li\n", (long int)VECTOR(D)[i]);
  }
  fclose(file);

  igraph_destroy(&G);
  igraph_vector_destroy(&D);

  return 0;
}

Condensed matter dataset
========================

The file ``cond-mat-2005.gml`` contains an updated version of
``cond-mat.gml``, the collaboration network of scientists posting
preprints on the condensed matter archive at www.arxiv.org.  This
version is based on preprints posted to the archive between January 1,
1995 and March 31, 2005.  The network is weighted, with weights
assigned as described in [Newman2001a]_.

These data can be cited (as an updated version of)

.. [Newman2001a]
   M. E. J. Newman. Scientific collaboration networks: I. Network
   construction and fundamental results. *Physical Review E*,
   64(1):016131, 2001, doi:10.1103/PhysRevE.64.016131.

.. [Newman2001b]
   M. E. J. Newman. The structure of scientific collaboration
   networks. *Proceedings of the National Academy of Sciences USA*,
   98(2):404--409, 2001, doi:10.1073/pnas.98.2.404.
